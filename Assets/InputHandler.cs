﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SG
{
    public class InputHandler : MonoBehaviour
    {
        public float horizontal;
        public float vertical;
        public float moveAmount;
        public float mosueX;
        public float mosueY;
        PlayerControls inputActions;

        Vector2 movementInput;
        Vector2 cameraInput;

         /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        public void OnEnable()
        {
            if(inputActions==null){
                    inputActions=new PlayerControls();
                    inputActions.Player_Movement.Movement.performed+=inputActions=> movementInput=inputActions.ReadValue<Vector2>();
                    inputActions.Player_Movement.Camera.performed+=i=> cameraInput=i.ReadValue<Vector2>();
            }

            inputActions.Enable();
        }

         /// <summary>
        /// This function is called when the behaviour becomes disabled or inactive.
        /// </summary>
        private void OnDisable()
        {
            inputActions.Disable();
        }
        public void TickInput(float delta){
             MoveInput(delta);
        }

        private void MoveInput(float delta){
            horizontal=movementInput.x;
            vertical=movementInput.y;
            moveAmount=Mathf.Clamp01(Mathf.Abs(horizontal)+Mathf.Abs(vertical));
            mosueX=cameraInput.x;
            mosueY=cameraInput.y;
        }
    }
}

